/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx.dnd;

import java.awt.Point;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.TransferHandler;
import xyz.znix.openlx.LightingPlanArea;
import xyz.znix.openlx.SingleLightTreeNode;
import static xyz.znix.openlx.dnd.LightListTransferHandler.LIGHT_FLAVOR;
import xyz.znix.openlx.items.Item;

/**
 *
 * @author Campbell Suter
 */
public class MainPanelTransferHandler extends TransferHandler {

    private final LightingPlanArea main;

    public MainPanelTransferHandler(LightingPlanArea main) {
        this.main = main;
    }

    @Override
    public boolean canImport(TransferSupport support) {
        return support.isDataFlavorSupported(LIGHT_FLAVOR);
    }

    @Override
    public boolean importData(TransferSupport support) {
        try {
            SingleLightTreeNode data = (SingleLightTreeNode) support
                    .getTransferable().getTransferData(LIGHT_FLAVOR);

            Point dropPoint = support.getDropLocation().getDropPoint();

            Item item = data.getItem();
            main.addItem(item, dropPoint);
        } catch (UnsupportedFlavorException | IOException ex) {
            Logger.getLogger(MainPanelTransferHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

}
