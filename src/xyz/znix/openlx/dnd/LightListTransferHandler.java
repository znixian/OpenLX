/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx.dnd;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import xyz.znix.openlx.SingleLightTreeNode;

/**
 *
 * @author Campbell Suter
 */
public class LightListTransferHandler extends TransferHandler {

    public static final DataFlavor LIGHT_FLAVOR
            = new DataFlavor(LightListTransferHandler.class, "Light") {
        @Override
        public boolean isFlavorSerializedObjectType() {
            return false;
        }

    };

    @Override
    public int getSourceActions(JComponent c) {
        return COPY;
    }

    @Override
    protected Transferable createTransferable(JComponent c) {
        Object selected = ((JTree) c).getSelectionPath().getLastPathComponent();
        return new LightSelection((SingleLightTreeNode) selected);
    }

    @Override
    protected void exportDone(JComponent c, Transferable t, int action) {
    }

    private static class LightSelection implements Transferable {

        private static final DataFlavor[] FLAVORS = {
            LIGHT_FLAVOR
        };

        private final SingleLightTreeNode data;

        public LightSelection(SingleLightTreeNode data) {
            this.data = data;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors() {
            // returning flavors itself would allow client code to modify
            // our internal behavior
            return (DataFlavor[]) FLAVORS.clone();
        }

        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return flavor == LIGHT_FLAVOR;
        }

        @Override
        public Object getTransferData(DataFlavor flavor)
                throws UnsupportedFlavorException {
            if (flavor == LIGHT_FLAVOR) {
                return data;
            } else {
                throw new UnsupportedFlavorException(flavor);
            }
        }

        public void lostOwnership(Clipboard clipboard, Transferable contents) {
        }
    }

}
