/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx;

import xyz.znix.openlx.math.ExtMath;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.function.Consumer;
import javax.swing.JComponent;
import xyz.znix.openlx.items.Item;

/**
 *
 * @author Campbell Suter
 */
public class LightingPlanArea extends JComponent implements MouseListener,
        MouseMotionListener, MouseWheelListener, KeyListener {

    public static final int UNZOOMED_GRID_SIZE = 20;
    public static final int MAJOR_GRID_INTERVAL = 5;

    private final Point viewport = new Point();
    private double zoom = 1;
    private Item selected;
    private final ArrayList<Item> items;
    private final Point lastMouse = new Point();
    private final Translater translater = new Translater();

    private boolean isRotatePressed;

    public LightingPlanArea() {
        this.items = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            items.add(new Fresnel());
//        }
//        for (int i = 0; i < 10; i++) {
//            items.add(new LightBar());
//        }
    }

    @Override
    public void paint(Graphics g) {
        // cache some variables
        int width = getWidth();
        int height = getHeight();
        double cellSize = UNZOOMED_GRID_SIZE * zoom;

        // scaled zoom, in case we want to do anything with it.
        double scaledZoom = zoom;

        // fill the background
        g.setColor(Colours.blueprintbackground);
        g.fillRect(0, 0, width, height);

        // draw the vertical lines (ie, the x changes, y stays the same)
        // x is the index on screen, not absolute.
        // so 0 is the first line on the screen.
        for (int x = 0; x < width / cellSize; x++) {
            // find the index of the line
            // (the lines at point 0 are filled black)
            int i = ExtMath.alwaysRoundDown(viewport.x / cellSize + x);

            // depending on the line, set a different colour
            if (i == 0) {
                g.setColor(Colours.zeroline);
            } else if (i % MAJOR_GRID_INTERVAL == 0) {
                g.setColor(Colours.line);
            } else {
                g.setColor(Colours.subline);
            }

            // find the on-screen x position (lx, Line X)
            int lx = (int) ( // cast it to int
                    x * cellSize // Base X position
                    - viewport.x % cellSize // smooth it out, when the view
                    // is not exactly aligned to the grid
                    );
            g.drawLine(lx, 0, lx, height); // vert. lines
        }

        // very similar to above
        // for more indepth comments, see above
        // draw the horizontal lines
        for (int y = 0; y < height / cellSize; y++) {
            // find the index
            int i = ExtMath.alwaysRoundDown(viewport.y / cellSize + y);

            // index based colour
            if (i == 0) {
                g.setColor(Colours.zeroline);
            } else if (i % MAJOR_GRID_INTERVAL == 0) {
                g.setColor(Colours.line);
            } else {
                g.setColor(Colours.subline);
            }

            // line screen-y
            int ly = (int) (y * cellSize - viewport.y % cellSize);
            g.drawLine(0, ly, width, ly); // vert. lines
        }

        translater.privateZoom = scaledZoom;
        items.stream().forEach((item) -> {
            g.setColor(item == selected ? Color.white : Color.black);
            item.paint((Graphics2D) g, translater, scaledZoom);
        });
    }

    public void addItem(Item item, Point dropPoint) {
        lastMouse.setLocation(dropPoint);
        items.add(item);
        item.drag(lastMouse.x + viewport.x, lastMouse.y + viewport.y, zoom, items);
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        lastMouse.setLocation(e.getPoint());
        int x = e.getX();
        int y = e.getY();
        selected = null;
        items.stream().filter((item) -> (item.isInside(x, y))).forEach((item) -> {
            selected = item;
        });
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        requestFocus();
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (selected == null) {
            viewport.x -= e.getX() - lastMouse.x;
            viewport.y -= e.getY() - lastMouse.y;
        } else {
            selected.drag(e.getX() - lastMouse.x, e.getY() - lastMouse.y, zoom, items);
        }
        lastMouse.setLocation(e.getPoint());
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        lastMouse.setLocation(e.getPoint());
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(20, 20);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (selected != null && isRotatePressed) {
            selected.rotate(Math.PI * 2 / 32 * e.getUnitsToScroll()
                    / Math.abs(e.getUnitsToScroll()), items); // 1/32 of a rotation
            repaint();
            return;
        }

        double mouseScreenX = e.getX() + viewport.x;
        double mouseScreenY = e.getY() + viewport.y;

        mouseScreenX /= zoom;
        mouseScreenY /= zoom;

        zoom *= Math.pow(0.95, e.getWheelRotation());
        if (zoom < 0.15) {
            zoom = 0.15;
        } else if (zoom > 15) {
            zoom = 15;
        }

        mouseScreenX *= zoom;
        mouseScreenY *= zoom;

        mouseScreenX -= e.getX();
        mouseScreenY -= e.getY();

        viewport.x = (int) mouseScreenX;
        viewport.y = (int) mouseScreenY;
        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_R:
                isRotatePressed = true;
                break;
            case KeyEvent.VK_BACK_SPACE:
            case KeyEvent.VK_DELETE:
                if (selected != null) {
                    items.remove(selected);
                    selected = null;
                    repaint();
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_R) {
            isRotatePressed = false;
        }
    }

    private class Translater implements Consumer<Point> {

        public double privateZoom;

        @Override
        public void accept(Point t) {
            t.x *= privateZoom;
            t.y *= privateZoom;
            t.x -= viewport.x;
            t.y -= viewport.y;
        }
    }
}
