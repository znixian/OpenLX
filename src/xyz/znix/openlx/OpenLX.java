/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import xyz.znix.openlx.dnd.MainPanelTransferHandler;

/**
 *
 * @author Campbell Suter
 */
public class OpenLX {

    /**
     * @param args the command line arguments
     * @throws org.newdawn.slick.SlickException
     */
    public static void main(String[] args) {
        LightingPlanArea area = new LightingPlanArea();

        MainPanel mainPanel = new MainPanel();
        mainPanel.getMainPanel().add(area);
        area.addMouseListener(area);
        area.addMouseMotionListener(area);
        area.addMouseWheelListener(area);
        area.addKeyListener(area);
        area.setTransferHandler(new MainPanelTransferHandler(area));
        area.setFocusable(true);

        JFrame frame = new JFrame("OpenLX Lighting Design");
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setBounds(20, 20, 200, 200);
        frame.setContentPane(mainPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowStateListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
            }
        });
    }

}
