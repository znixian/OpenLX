/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import javax.swing.tree.TreeNode;
import xyz.znix.openlx.items.Fresnel;
import xyz.znix.openlx.items.LightBar;

/**
 *
 * @author Campbell Suter
 */
public class LightListTreeNode implements TreeNode {

    private final List<SingleLightTreeNode> nodes;

    public LightListTreeNode() {
        this.nodes = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            SingleLightTreeNode node
                    = new SingleLightTreeNode(this, "Light " + i, () -> {
                        return new Fresnel();
                    });
            nodes.add(node);
        }

        SingleLightTreeNode bar = new SingleLightTreeNode(this, "Lighting Bar", () -> {
            return new LightBar();
        });
        nodes.add(bar);
    }

    @Override
    public TreeNode getChildAt(int childIndex) {
        return nodes.get(childIndex);
    }

    @Override
    public int getChildCount() {
        return nodes.size();
    }

    @Override
    public TreeNode getParent() {
        return null;
    }

    @Override
    public int getIndex(TreeNode node) {
        return nodes.indexOf(node);
    }

    @Override
    public boolean getAllowsChildren() {
        return true;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    @Override
    public Enumeration children() {
        Iterator<SingleLightTreeNode> iterator = nodes.iterator();
        return new Enumeration() {
            @Override
            public boolean hasMoreElements() {
                return iterator.hasNext();
            }

            @Override
            public Object nextElement() {
                return iterator.next();
            }
        };
    }

    @Override
    public String toString() {
        return "Lights";
    }

}
