/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx;

import java.util.Enumeration;
import java.util.function.Supplier;
import javax.swing.tree.TreeNode;
import xyz.znix.openlx.items.Item;

/**
 *
 * @author Campbell Suter
 */
public class SingleLightTreeNode implements TreeNode {

    private final LightListTreeNode parent;
    private final String name;
    private final Supplier<Item> lightSource;

    public SingleLightTreeNode(LightListTreeNode parent, String name, Supplier<Item> lightSource) {
        this.parent = parent;
        this.name = name;
        this.lightSource = lightSource;
    }

    @Override
    public String toString() {
        return name;
    }

    public Item getItem() {
        return lightSource.get();
    }

    @Override
    public TreeNode getChildAt(int childIndex) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getChildCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public TreeNode getParent() {
        return parent;
    }

    @Override
    public int getIndex(TreeNode node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean getAllowsChildren() {
        return false;
    }

    @Override
    public boolean isLeaf() {
        return true;
    }

    @Override
    public Enumeration children() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
