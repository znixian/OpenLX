/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx.items;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.function.Consumer;

/**
 *
 * @author Campbell Suter
 */
public class Fresnel extends Item {

    @Override
    public void paint(Graphics2D g, Consumer<Point> translater, double zoom) {
        super.paint(g, translater, zoom);
        paintShape(g, zoom);
    }

    @Override
    public int getDefaultWidth() {
        return 60;
    }

    @Override
    public int getDefaultHeight() {
        return 30;
    }

    @Override
    public boolean snapsTo(SnapType type) {
        return type == SnapType.BARS; //  || type == SnapType.GRID_MAJOR
    }

    @Override
    public void makeShape() {
        shape.reset();

        int h = (int) getHeight();
        int w = (int) (getWidth() + 10);
        int lw = w - 10;

        shape.moveTo(0, 0);
        for (int yy = 0; yy < h; yy += 5) {
            if (yy > h) {
                yy = h;
            }
            shape.lineTo(yy % 10 == 0 ? w : lw, yy);
        }
        shape.lineTo(w, h);
        shape.lineTo(0, h);
        shape.lineTo(0, 0);
    }

}
