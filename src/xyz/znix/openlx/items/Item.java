/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx.items;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import static xyz.znix.openlx.LightingPlanArea.MAJOR_GRID_INTERVAL;
import static xyz.znix.openlx.LightingPlanArea.UNZOOMED_GRID_SIZE;
import xyz.znix.openlx.math.LineDistFinder;
import xyz.znix.openlx.math.LinePointFinder;

/**
 *
 * @author Campbell Suter
 */
public abstract class Item {

    protected static final AffineTransform TMP = new AffineTransform();

    protected double rotation;

    public static final int MAX_SNAP_DISTANCE = 30;
    public static final int MAX_SNAP_DISTANCE_SQU
            = MAX_SNAP_DISTANCE * MAX_SNAP_DISTANCE;

    protected final Rectangle2D.Double boundingBox = new Rectangle2D.Double(0, 0, getDefaultWidth(), getDefaultHeight());
    protected final Path2D.Double shape = new Path2D.Double();

    protected final Point pos = new Point();
    protected final Point size = new Point();

    private double dragX, dragY;

    private Item snappedTo;

    public void paint(Graphics2D g, Consumer<Point> translater, double zoom) {
        pos.x = (int) boundingBox.getX();
        pos.y = (int) boundingBox.getY();
        translater.accept(pos);

        size.x = (int) boundingBox.getWidth();
        size.y = (int) boundingBox.getHeight();

        size.x *= zoom;
        size.y *= zoom;

        makeShape();
        TMP.setToIdentity();
        TMP.translate(pos.x, pos.y);
        TMP.rotate(rotation);
        TMP.scale(zoom, zoom);
        shape.transform(TMP);
    }

    public void paintShape(Graphics2D g, double zoom) {
        g.draw(shape);
    }

    public boolean isInside(int x, int y) {
        return shape.contains(x, y);
    }

    public void drag(double x, double y, double zoom, List<Item> items) {
        dragX += x / zoom;
        dragY += y / zoom;

        double bbx = boundingBox.x;
        double bby = boundingBox.y;

        moveTo(dragX, dragY, zoom, items);

        items.stream().filter((item) -> (item != this && item.snappedTo == this)).forEach((item) -> {
            item.drag(boundingBox.x - bbx, boundingBox.y - bby, zoom, items);
        });
    }

    public void moveTo(double x, double y, double zoom, List<Item> items) {
        boundingBox.x = x;
        boundingBox.y = y;
        snapTo(items);
    }

    protected void snapTo(List<Item> items) {
        snappedTo = null;

        if (snapsTo(SnapType.BARS)) {
            // we are calling distanceToBar FAR more then we need to, for
            // sake of code size. It is very fast, though, so it doesn't
            // really matter.
            Optional<Item> reduce = items.stream().filter((item) -> (item instanceof LightBar)).reduce((t, u) -> {
                return distanceToBar((LightBar) t) > distanceToBar((LightBar) u) ? u : t;
            });
            if (reduce.isPresent()) {
                Item best = reduce.get();
                if (distanceToBar((LightBar) best) < MAX_SNAP_DISTANCE) {
//                    boundingBox.x = best.boundingBox.x - getWidth() / 2;
                    double x1 = best.getX();
                    double y1 = best.getY();
                    double x2 = x1 + Math.sin(-best.rotation) * best.getHeight();
                    double y2 = y1 + Math.cos(-best.rotation) * best.getHeight();

                    Point2D.Double p1 = new Point2D.Double(x1, y1);
                    Point2D.Double p2 = new Point2D.Double(x2, y2);
                    Point2D.Double pos2 = new Point2D.Double(getX() + getWidth() / 2, getY());
                    Point2D.Double snapTo = LinePointFinder.getClosestPointOnSegment(p1, p2, pos2);
                    boundingBox.x = snapTo.x - getWidth() / 2;
                    boundingBox.y = snapTo.y;

                    snappedTo = best;
                    return;
                }
            }
        }

        if (snapsTo(SnapType.GRID_MINOR)) {
            snapToGrid(UNZOOMED_GRID_SIZE);
        } else if (snapsTo(SnapType.GRID_MAJOR)) {
            snapToGrid(UNZOOMED_GRID_SIZE * MAJOR_GRID_INTERVAL);
        }
    }

    private double distanceToBar(LightBar bar) {
        int hwidth = (int) (getWidth() / 2);
        int hwidthBar = (int) (bar.getWidth() / 2);

        double x1 = bar.getX();
        double y1 = bar.getY();
        double x2 = x1 + Math.sin(-bar.rotation) * bar.getHeight();
        double y2 = y1 + Math.cos(-bar.rotation) * bar.getHeight();

//        double bx = bar.getX() + hwidthBar;
//        double y1 = bar.getY();
//        double y2 = y1 + bar.getHeight();
        return LineDistFinder.distanceToSegment(boundingBox.x + hwidth, boundingBox.y, x1, y1, x2, y2);
    }

    /**
     * Snap this item to a grid
     *
     * @param gridSize The size of the grid
     */
    private void snapToGrid(int gridSize) {
        // cache variables
        int halfWidth = (int) (getWidth() / 2);
        int halfHeight = (int) (getHeight() / 2);

        // find the closest snap-to point
        double nx = Math.round((boundingBox.x + halfWidth) / gridSize) * gridSize - halfWidth;
        double ny = Math.round((boundingBox.y + halfHeight) / gridSize) * gridSize - halfHeight;

        // find the X and Y distance to it
        double dx = Math.abs(nx - boundingBox.x);
        double dy = Math.abs(ny - boundingBox.y);

        // via pythag, is this close enough
        if (dx * dx + dy * dy < MAX_SNAP_DISTANCE_SQU) {
            boundingBox.x = nx;
            boundingBox.y = ny;
        }
    }

    public double getX() {
        return boundingBox.x;
    }

    public void setX(double x) {
        boundingBox.x = x;
    }

    public double getY() {
        return boundingBox.y;
    }

    public void setY(double y) {
        boundingBox.y = y;
    }

    public double getWidth() {
        return boundingBox.width;
    }

    public void setWidth(double width) {
        boundingBox.width = width;
    }

    public double getHeight() {
        return boundingBox.height;
    }

    public void setHeight(double height) {
        boundingBox.height = height;
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation, List<Item> items) {
        double diff = rotation - this.rotation;
        this.rotation = rotation;

        items.stream().filter((item) -> (item != this && item.snappedTo == this)).forEach((item) -> {
            item.setRotation(item.getRotation() + diff, items);
            double xdiff = item.getX() - getX();
            double ydiff = item.getY() - getY();
            double dist = Math.sqrt(xdiff * xdiff + ydiff * ydiff);
            double angle = Math.tanh(xdiff / ydiff);
            angle -= diff;
            xdiff = Math.sin(angle) * dist;
            ydiff = Math.cos(angle) * dist;
            item.setX(xdiff + getX());
            item.setY(ydiff + getY());
            item.snapTo(items);
        });
    }

    public void rotate(double d, List<Item> items) {
        setRotation(d + getRotation(), items);
    }

    public abstract int getDefaultWidth();

    public abstract int getDefaultHeight();

    public abstract boolean snapsTo(SnapType type);

    public abstract void makeShape();
}
