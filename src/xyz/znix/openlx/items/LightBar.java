/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx.items;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.function.Consumer;

/**
 *
 * @author Campbell Suter
 */
public class LightBar extends Item {

    @Override
    public int getDefaultWidth() {
        return 20;
    }

    @Override
    public int getDefaultHeight() {
        return 500;
    }

    @Override
    public void paint(Graphics2D g, Consumer<Point> translater, double zoom) {
        super.paint(g, translater, zoom);
        paintShape(g, zoom);
//        
//        int x1 = (int) getX();
//        int y1 = (int) getY();
//        int x2 = (int) (x1 + Math.sin(-rotation) * getHeight());
//        int y2 = (int) (y1 + Math.cos(-rotation) * getHeight());
//        g.drawLine(x1, y1, x2, y2);
    }

    @Override
    public boolean snapsTo(SnapType type) {
        return type == SnapType.GRID_MAJOR || type == SnapType.GRID_MINOR;
    }

    @Override
    public void makeShape() {
        shape.reset();
        shape.moveTo(0, 0);
        shape.lineTo(getWidth(), 0);
        shape.lineTo(getWidth(), getHeight());
        shape.lineTo(0, getHeight());
        shape.lineTo(0, 0);
    }

}
