/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.openlx;

import java.awt.Color;

/**
 *
 * @author Campbell Suter
 */
public final class Colours {

    public static final Color blueprintbackground = new Color(100, 100, 255);
    public static final Color subline = new Color(125, 125, 255);
    public static final Color line = new Color(150, 150, 255);
    public static final Color zeroline = new Color(0, 0, 0);

    private Colours() {
    }
}
